%define SYS_EXIT 60
%define ONE_VAL 1
section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, SYS_EXIT
	xor rdi, rdi
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov  rax, rdi
    .counter:
        cmp  byte [rdi], 0
        je   .end
        inc  rdi
        jmp  .counter
    .end:
        sub  rdi, rax
        mov  rax, rdi
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    ; получаем длину строки
    call string_length
    mov rdx, rax
    ; выводим содержимое
    pop rsi 
    mov  rax, ONE_VAL
    mov  rdi, ONE_VAL
    syscall
    inc rdi

    ret

; Принимает код символа и выводит его в stdout
print_char:

	push rdi
	mov rsi, rsp ;указываем на символ, который положили в стек
	pop rdi
	mov rax, ONE_VAL
	mov rdi, ONE_VAL
	mov rdx, ONE_VAL
	syscall

	ret

; Переводит строку (выводит символ с кодом 0xA)
; Done
print_newline:
    mov rdi, '\n'
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi

    mov rsi, rsp

    dec rsi
    mov byte[rsi], 0 ; помещаем ноль, чтобы позже знать до какого места следует считывать символы
    .loop:
        xor rdx, rdx ; перед делением необходимо обнулять значение rdx (иначе может получиться непредсказуемый результат)
        mov rdi, 10 ; указываем делитель
        div rdi 
        add rdx, '0'; Добавляем код нуля

        dec rsi ; уменьшаем указатель стека для следующего символа

        mov byte[rsi], dl ; помещаем младший байт из регистра, в котором сохраняется остаток в стек

        test rax, rax ; проверяем, можно ли еще осуществить деление

        jne .loop

    .print_number:
        mov rdi, rsi ; помещаем в rdi указатель на вершину стека

        lea rsp, [rsp-64] ; перемещаем указатель стека для того, чтобы случайно не затереть нашу строку
        call print_string ; вызываем функцию, которая будет идти по стеку и выводить символы до тех пор, пока не встретит 0
        lea rsp, [rsp+64]
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .less
    jmp print_uint
    .less:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        call print_uint
        ret 

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:

    mov r8, rdi
    mov r9, rsi
    mov rax, 0
    .loop:
        mov dil, [r8+rax]
        mov sil, [r9+rax]

        cmp dil, sil ; делаем сравнение символов по кодам. Если не равны, выход с 0 кодом
        jne .error

        cmp dil, 0 ; теперь когда символы точно равны, проверяем, конец ли это строки и выходим с кодом 1
        je .equal 

        inc rax ; если ни то ни другое условие не выполнилось, то переходим к следующему символу, увеличивая адрес
        jmp .loop
        
    .error:
        mov rax, 0
        ret
    .equal:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:

    xor rax, rax ; системный вызов read
    xor rdi, rdi ; считываение из stdin
    lea rsi, [rsp-1] ; запишем наш символ в ячейке над вершиной стека(lea получит адрес rsp-1)
    mov rdx, 1 ; считываем 1 символ
    syscall

    test rax, rax ; показывает количество прочитанных символов(если равно 0, то мы достигли конца потока)
    ; это нельзя заменить на проверку символа в ячейке rsp-1, т.к. туда ничего не записалось, и, возможно, там находилось значение
    ; отличное от 0
    je .null
    

    mov al, byte[rsp-1] ; загружаем прочитанный символ в al (младший байт rax)
    ret

    .null:
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    


    push r12 ; 
    push r13 ; 
    push r14 ; 

    mov r12, rdi ; указатель на текущий символ
    mov r13, rsi ; оставшийся размер буфера
    mov r14, rsi ; размер буфера

    .unnecessary_characters: ; считываем символы до тех пор, пока не встретится символ помимо пробела, табуляции или перевода строки
        xor rax, rax

        call read_char

        cmp rax, ' '
        je .unnecessary_characters

        cmp rax, 0x9
        je .unnecessary_characters

        cmp rax, 0xA
        je .unnecessary_characters

    .reading_word:
        ; На этом этапе у нас имеется прочитанный символ
        
        cmp rax, ' '
        je .exit

        cmp rax, 0x9
        je .exit

        cmp rax, 0
        je .exit

        cmp rax, 0xA
        je .exit
    
        cmp r13, 0
        je .err_exit

        mov byte[r12], al ; записываем прочитанный символ
        inc r12 ; перемещаем указатель на следующий байт
        dec r13 ; уменьшаем размер буфера


        call read_char

        jmp .reading_word

    .exit:
        mov byte[r12], 0
        
        sub r14, r13 ; получаем длину слова 

        sub r12, r14 ; получаем указатель на начало буфера

        mov rax, r12 ; перемещаем адрес на строку

        mov rdx, r14 ; получаем длину слова
        

        pop r14
        pop r13
        pop r12

        ret

    .err_exit:
        xor rax, rax

        pop r14
        pop r13
        pop r12

        ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r8, 0 ; счётчик символов
    mov r9, 10
    xor rax, rax
    .loop:
        xor rsi, rsi
        mov sil, byte[rdi+r8]
        
        sub sil, '0' ; вычитаем код символа '0'

        cmp sil, 9
        jg .exit ; проверяем, является ли этот символ числом (т.е. в диапазоне от 0 до 10)

        cmp sil, 0
        jl .exit ; проверяем, является ли этот символ числом (т.е. в диапазоне от 0 до 10)

        mul r9
        add rax, rsi
        
        inc r8

        jmp .loop
    .exit:
        mov rdx, r8
        ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp rax, '-' ; проверка на минус
    jne .positive_number
    cmp rax, '+' ; проверка на плюс
    je .skip_plus



    .negative_number:
        inc rdi
        call parse_uint
        inc rdx ; учитываем минус
        neg rax
        ret

    .skip_plus:
        inc rdi
    .positive_number:
        jmp parse_uint
 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:

    ;rdi-указатель на строку, rsi-указатель на буфер, rdx-длина буфера
    .check_string_size:
        push rdi
        push rsi
        push rdx
        call string_length; rax=Длина строки
        inc rax ; увеличиваем длину, учитывая, что нужно записать 0(конец строки)
        pop rdx
        pop rsi
        pop rdi

        cmp rax, rdx
        jg .err_exit
        
        mov r8, rax ; количество символов строки, которое осталось поместить в буфер
        push rax
        
    .loop:
        test r8, r8
        je .exit

        mov al, byte[rdi] ; посимвольно помещаем значения из строки в буфер
        mov byte[rsi], al

        inc rsi
        inc rdi

        dec r8
        jmp .loop
    .exit:
        pop rax ; возвращаем значение длины строки 
        ret
    .err_exit:
        xor rax, rax
        ret




